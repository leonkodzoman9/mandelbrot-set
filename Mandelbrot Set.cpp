#include "Includes.h"

#include "Window.h"
#include "Mouse.h"
#include "Keyboard.h"

#include "Timer.h"

#include "Vector.h"

#include "ImplementationsCPU.h"
#include "ImplementationsGPU.cuh"



enum class Precision {
	F32,
	F64
};

enum class Implementation {
	NORMAL,
	SSE2,
	AVX2,
	CUDA
};

enum class SetType {
	MANDELBROT,
	MULTIBROT
};

std::string getPrecisionName(Precision precision) {
	switch (precision) {
	case Precision::F32: return "F32";
	case Precision::F64: return "F64";
	}
	return "None";
}
std::string getImplementationName(Implementation implementation) {
	switch (implementation) {
	case Implementation::NORMAL: return "Normal";
	case Implementation::SSE2: return "SSE2";
	case Implementation::AVX2: return "AVX2";
	case Implementation::CUDA: return "CUDA";
	}
	return "None";
}
std::string getSetTypeName(SetType setType) {
	switch (setType) {
	case SetType::MANDELBROT: return "Mandelbrot";
	case SetType::MULTIBROT: return "Multibrot";
	}
	return "None";
}



int main() {

	Vector2i wSize(800, 800);
	std::string title = "Mandelbrot Set";

	Window& window = Window::getInstance();
	Mouse& mouse = Mouse::getInstance();
	Keyboard& keyboard = Keyboard::getInstance();

	window.initialize(wSize, title);
	glfwSwapInterval(0);

	int pixelCount = window.size.x * window.size.y;

	
	
	int maxIterations = 64;
	Vector4d area(-2.5, 2, 1.5, -2);

	Precision precision = Precision::F32;
	Implementation implementation = Implementation::NORMAL;
	SetType setType = SetType::MANDELBROT;

	float power = 4;

	bool parallel = false;


	int* iterationsPtr;
	cudaMallocHost(&iterationsPtr, pixelCount * sizeof(int));

	std::span iterations(iterationsPtr, pixelCount);



	std::vector<Vector3f> pixels(window.size.x * window.size.y);

	MultiTimer frametimeTimer(50);
	MultiTimer executionTimer(50);
	while (!glfwWindowShouldClose(window.getWindow())) {

		frametimeTimer.update();

		std::string frametime = std::to_string(frametimeTimer.getAverageInterval()) + " ms";
		std::string execution = std::to_string(executionTimer.getAverageInterval()) + " ms";
		std::string typeString = getSetTypeName(setType);
		std::string powerString = std::to_string(power);
		std::string implString = getImplementationName(implementation);
		std::string precString = getPrecisionName(precision);
		std::string iterString = std::to_string(maxIterations);
		std::string isParallel = parallel || implementation == Implementation::CUDA ? "Parallel" : "Sequential";

		window.setTitle(title + " : " + frametime + ", " + execution + ", " + typeString + ", " + implString + ", " + precString + ", " + iterString + ", " + isParallel + ", " + powerString);

		glfwPollEvents();

		mouse.update();
		keyboard.update();

		if (keyboard.held(KeyCode::W)) {
			double dy = area.w - area.y;
			area.y -= dy / 300;
			area.w -= dy / 300;
		}
		if (keyboard.held(KeyCode::A)) {
			double dx = area.z - area.x;
			area.x -= dx / 200;
			area.z -= dx / 200;
		}
		if (keyboard.held(KeyCode::S)) {
			double dy = area.w - area.y;
			area.y += dy / 300;
			area.w += dy / 300;
		}
		if (keyboard.held(KeyCode::D)) {
			double dx = area.z - area.x;
			area.x += dx / 200;
			area.z += dx / 200;
		}
		if (keyboard.held(KeyCode::Q)) {

			Vector2d center = Vector2d(area.x + area.z, area.y + area.w) / 2;
			Vector2d sizeHalf = Vector2d(area.z - area.x, area.w - area.y) / 2 * 0.97;

			area.x = center.x - sizeHalf.x;
			area.z = center.x + sizeHalf.x;
			area.y = center.y + sizeHalf.x;
			area.w = center.y - sizeHalf.x;
		}
		if (keyboard.held(KeyCode::E)) {

			Vector2d center = Vector2d(area.x + area.z, area.y + area.w) / 2;
			Vector2d sizeHalf = Vector2d(area.z - area.x, area.w - area.y) / 2 / 0.97;

			area.x = center.x - sizeHalf.x;
			area.z = center.x + sizeHalf.x;
			area.y = center.y + sizeHalf.x;
			area.w = center.y - sizeHalf.x;
		}

		if (keyboard.pressed(KeyCode::P)) {
			precision = precision == Precision::F32 ? Precision::F64 : Precision::F32;
		}
		if (keyboard.pressed(KeyCode::R)) {

			maxIterations = 64;
			area = Vector4f(-2.5, 2, 1.5, -2);

			precision = Precision::F32;
			implementation = Implementation::NORMAL;
			setType = SetType::MANDELBROT;
		}

		if (keyboard.pressed(KeyCode::UP) || keyboard.heldFor(KeyCode::UP) > 300) {
			maxIterations += 64;
		}
		if (keyboard.pressed(KeyCode::DOWN) || keyboard.heldFor(KeyCode::DOWN) > 300) {
			maxIterations -= maxIterations > 64 ? 64 : 0;
		}

		if (keyboard.held(KeyCode::LEFT)) {
			power -= 0.005;
		}
		if (keyboard.held(KeyCode::RIGHT)) {
			power += 0.005;
		}

		if (keyboard.pressed(KeyCode::M)) {
			parallel = !parallel;
		}
		if (keyboard.pressed(KeyCode::T)) {
			setType = setType == SetType::MANDELBROT ? SetType::MULTIBROT : SetType::MANDELBROT;
		}

		if (keyboard.pressed(KeyCode::NUM_1)) { implementation = Implementation::NORMAL; }
		if (keyboard.pressed(KeyCode::NUM_2)) { implementation = Implementation::SSE2; }
		if (keyboard.pressed(KeyCode::NUM_3)) { implementation = Implementation::AVX2; }
		if (keyboard.pressed(KeyCode::NUM_4)) { implementation = Implementation::CUDA; }

		glClear(GL_COLOR_BUFFER_BIT);

		

		executionTimer.stop();

		if (setType == SetType::MANDELBROT) {
			if (precision == Precision::F32) {
				switch (implementation) {
				case Implementation::NORMAL:	Mandelbrot::Normal<float>(iterations, area, maxIterations, parallel); break;
				case Implementation::SSE2:		Mandelbrot::SSE2<float>(iterations, area, maxIterations, parallel); break;
				case Implementation::AVX2:		Mandelbrot::AVX2<float>(iterations, area, maxIterations, parallel); break;
				case Implementation::CUDA:		Mandelbrot::CUDA(iterationsPtr, pixelCount, { area.x, area.y, area.z, area.w }, maxIterations, { wSize.x, wSize.y }, false); break;
				}
			}
			else {
				switch (implementation) {
				case Implementation::NORMAL:	Mandelbrot::Normal<double>(iterations, area, maxIterations, parallel); break;
				case Implementation::SSE2:		Mandelbrot::SSE2<double>(iterations, area, maxIterations, parallel); break;
				case Implementation::AVX2:		Mandelbrot::AVX2<double>(iterations, area, maxIterations, parallel); break;
				case Implementation::CUDA:		Mandelbrot::CUDA(iterationsPtr, pixelCount, { area.x, area.y, area.z, area.w }, maxIterations, { wSize.x, wSize.y }, true); break;
				}
			}
		}
		else {
			if (precision == Precision::F32) {
				switch (implementation) {
				case Implementation::NORMAL:	Multibrot::Normal<float>(iterations, area, maxIterations, power, parallel); break;
				case Implementation::SSE2:		Multibrot::SSE2<float>(iterations, area, maxIterations, power, parallel); break;
				case Implementation::AVX2:		Multibrot::AVX2<float>(iterations, area, maxIterations, power, parallel); break;
				case Implementation::CUDA:		Multibrot::CUDA(iterationsPtr, pixelCount, { area.x, area.y, area.z, area.w }, maxIterations, { wSize.x, wSize.y }, power, false); break;
				}
			}
			else {
				switch (implementation) {
				case Implementation::NORMAL:	Multibrot::Normal<double>(iterations, area, maxIterations, power, parallel); break;
				case Implementation::SSE2:		Multibrot::SSE2<double>(iterations, area, maxIterations, power, parallel); break;
				case Implementation::AVX2:		Multibrot::AVX2<double>(iterations, area, maxIterations, power, parallel); break;
				case Implementation::CUDA:		Multibrot::CUDA(iterationsPtr, pixelCount, { area.x, area.y, area.z, area.w }, maxIterations, { wSize.x, wSize.y }, power, true); break;
				}
			}
		}

		executionTimer.update();



		for (int i = 0; i < wSize.y; i++) {
			for (int j = 0; j < wSize.x; j++) {
				
				int readIndex = i * wSize.x + j;
				int writeIndex = (wSize.y - i - 1) * wSize.x + j;

				float percent = (float)iterations[readIndex] / (float)maxIterations;

				if (percent >= 1.0f) {
					pixels[writeIndex] = Vector3f(0, 0, 0);
				}
				else {
					pixels[writeIndex] = Vector3f(percent, 1 - percent, 0);
				}
			}
		}

		glDrawPixels(wSize.x, wSize.y, GL_RGB, GL_FLOAT, pixels.data());

		glfwSwapBuffers(Window::getInstance().getWindow());
	}

	cudaFreeHost(iterationsPtr);

	return 0;
}