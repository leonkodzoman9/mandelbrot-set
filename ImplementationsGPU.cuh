#include "Includes.h"

namespace Mandelbrot {

	void CUDA(int* iterations, int pixelCount, double4 area, int maxIterations, int2 wSize, bool doublePrecision);
}

namespace Multibrot {

	void CUDA(int* iterations, int pixelCount, double4 area, int maxIterations, int2 wSize, float power, bool doublePrecision);
}
