#pragma once

#include "Includes.h"

#include "Vector.h"

#include <immintrin.h>



namespace Mandelbrot {

	template <class Type>
	void Normal(std::span<int> iterations, Vector4d area, int maxIterations, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		Type deltaX = (area.z - area.x) / wSize.x;
		Type deltaY = (area.w - area.y) / wSize.y;

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			Type x = 0;
			Type y = 0;

			Type x0 = area.x + deltaX * j;
			Type y0 = area.y + deltaY * i;

			int iteration = 0;
			while (x * x + y * y <= 4 && iteration < maxIterations) {

				Type nx = x * x - y * y + x0;
				Type ny = 2 * x * y + y0;

				x = nx;
				y = ny;

				iteration++;
			}

			maxIteration = iteration;
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}



	template <class Type> requires (std::is_same<Type, float>::value)
		void SSE2(std::span<int> iterations, Vector4d area, int maxIterations, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		__m128 _1 = _mm_set1_ps(1);
		__m128 _2 = _mm_set1_ps(2);
		__m128 _4 = _mm_set1_ps(4);
		__m128 _0123 = _mm_setr_ps(0, 1, 2, 3);

		__m128 _areaX = _mm_set1_ps(area.x);
		__m128 _areaY = _mm_set1_ps(area.y);
		__m128 _deltaX = _mm_set1_ps((area.z - area.x) / wSize.x);
		__m128 _deltaY = _mm_set1_ps((area.w - area.y) / wSize.y);
		__m128 _maxIterations = _mm_set1_ps(maxIterations);

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			if (j % 4 != 0) {
				return;
			}

			__m128 _i = _mm_set1_ps(i);

			__m128 _j = _mm_set1_ps(j);
			_j = _mm_add_ps(_j, _0123);

			__m128 _x = _mm_setzero_ps();
			__m128 _y = _mm_setzero_ps();

			__m128 _x0 = _mm_fmadd_ps(_deltaX, _j, _areaX);
			__m128 _y0 = _mm_fmadd_ps(_deltaY, _i, _areaY);

			__m128 _iteration = _mm_setzero_ps();
			while (true) {

				__m128 _x2 = _mm_mul_ps(_x, _x);
				__m128 _y2 = _mm_mul_ps(_y, _y);

				__m128 _condition1 = _mm_cmp_ps(_mm_add_ps(_x2, _y2), _4, _CMP_LE_OQ);
				__m128 _condition2 = _mm_cmp_ps(_iteration, _maxIterations, _CMP_LT_OQ);
				__m128 _loopCondition = _mm_and_ps(_condition1, _condition2);

				if (_mm_movemask_ps(_loopCondition) == 0) {
					break;
				}

				__m128 _nx = _mm_add_ps(_mm_sub_ps(_x2, _y2), _x0);
				__m128 _ny = _mm_fmadd_ps(_mm_mul_ps(_2, _x), _y, _y0);

				_x = _mm_blendv_ps(_x, _nx, _loopCondition);
				_y = _mm_blendv_ps(_y, _ny, _loopCondition);

				_iteration = _mm_add_ps(_iteration, _mm_and_ps(_1, _loopCondition));
			}

			__m128i _results = _mm_cvtps_epi32(_iteration);

			_mm_storeu_epi32(&maxIteration, _results);
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}

	template <class Type> requires (std::is_same<Type, double>::value)
		void SSE2(std::span<int> iterations, Vector4d area, int maxIterations, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		__m128d _1 = _mm_set1_pd(1);
		__m128d _2 = _mm_set1_pd(2);
		__m128d _4 = _mm_set1_pd(4);
		__m128d _01 = _mm_setr_pd(0, 1);

		__m128d _areaX = _mm_set1_pd(area.x);
		__m128d _areaY = _mm_set1_pd(area.y);
		__m128d _deltaX = _mm_set1_pd((area.z - area.x) / wSize.x);
		__m128d _deltaY = _mm_set1_pd((area.w - area.y) / wSize.y);
		__m128d _maxIterations = _mm_set1_pd(maxIterations);

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			if (j % 2 != 0) {
				return;
			}

			__m128d _i = _mm_set1_pd(i);

			__m128d _j = _mm_set1_pd(j);
			_j = _mm_add_pd(_j, _01);

			__m128d _x = _mm_setzero_pd();
			__m128d _y = _mm_setzero_pd();

			__m128d _x0 = _mm_fmadd_pd(_deltaX, _j, _areaX);
			__m128d _y0 = _mm_fmadd_pd(_deltaY, _i, _areaY);

			__m128d _iteration = _mm_setzero_pd();
			while (true) {

				__m128d _x2 = _mm_mul_pd(_x, _x);
				__m128d _y2 = _mm_mul_pd(_y, _y);

				__m128d _condition1 = _mm_cmp_pd(_mm_add_pd(_x2, _y2), _4, _CMP_LE_OQ);
				__m128d _condition2 = _mm_cmp_pd(_iteration, _maxIterations, _CMP_LT_OQ);
				__m128d _loopCondition = _mm_and_pd(_condition1, _condition2);

				if (_mm_movemask_pd(_loopCondition) == 0) {
					break;
				}

				__m128d _nx = _mm_add_pd(_mm_sub_pd(_x2, _y2), _x0);
				__m128d _ny = _mm_fmadd_pd(_mm_mul_pd(_2, _x), _y, _y0);

				_x = _mm_blendv_pd(_x, _nx, _loopCondition);
				_y = _mm_blendv_pd(_y, _ny, _loopCondition);

				_iteration = _mm_add_pd(_iteration, _mm_and_pd(_1, _loopCondition));
			}

			__m128i _results = _mm_cvtpd_epi32(_iteration);

			(&maxIteration)[0] = _results.m128i_i32[0];
			(&maxIteration)[1] = _results.m128i_i32[1];
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}



	template <class Type> requires (std::is_same<Type, float>::value)
		void AVX2(std::span<int> iterations, Vector4d area, int maxIterations, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		__m256 _1 = _mm256_set1_ps(1);
		__m256 _2 = _mm256_set1_ps(2);
		__m256 _4 = _mm256_set1_ps(4);
		__m256 _01234567 = _mm256_setr_ps(0, 1, 2, 3, 4, 5, 6, 7);

		__m256 _areaX = _mm256_set1_ps(area.x);
		__m256 _areaY = _mm256_set1_ps(area.y);
		__m256 _deltaX = _mm256_set1_ps((area.z - area.x) / wSize.x);
		__m256 _deltaY = _mm256_set1_ps((area.w - area.y) / wSize.y);
		__m256 _maxIterations = _mm256_set1_ps(maxIterations);

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			if (j % 8 != 0) {
				return;
			}

			__m256 _i = _mm256_set1_ps(i);

			__m256 _j = _mm256_set1_ps(j);
			_j = _mm256_add_ps(_j, _01234567);

			__m256 _x = _mm256_setzero_ps();
			__m256 _y = _mm256_setzero_ps();

			__m256 _x0 = _mm256_fmadd_ps(_deltaX, _j, _areaX);
			__m256 _y0 = _mm256_fmadd_ps(_deltaY, _i, _areaY);

			__m256 _iteration = _mm256_setzero_ps();
			while (true) {

				__m256 _x2 = _mm256_mul_ps(_x, _x);
				__m256 _y2 = _mm256_mul_ps(_y, _y);

				__m256 _condition1 = _mm256_cmp_ps(_mm256_add_ps(_x2, _y2), _4, _CMP_LE_OQ);
				__m256 _condition2 = _mm256_cmp_ps(_iteration, _maxIterations, _CMP_LT_OQ);
				__m256 _loopCondition = _mm256_and_ps(_condition1, _condition2);

				if (_mm256_movemask_ps(_loopCondition) == 0) {
					break;
				}

				__m256 _nx = _mm256_add_ps(_mm256_sub_ps(_x2, _y2), _x0);
				__m256 _ny = _mm256_fmadd_ps(_mm256_mul_ps(_2, _x), _y, _y0);

				_x = _mm256_blendv_ps(_x, _nx, _loopCondition);
				_y = _mm256_blendv_ps(_y, _ny, _loopCondition);

				_iteration = _mm256_add_ps(_iteration, _mm256_and_ps(_1, _loopCondition));
			}

			__m256i _results = _mm256_cvtps_epi32(_iteration);

			_mm256_storeu_epi32(&maxIteration, _results);
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}

	template <class Type> requires (std::is_same<Type, double>::value)
		void AVX2(std::span<int> iterations, Vector4d area, int maxIterations, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		__m256d _1 = _mm256_set1_pd(1);
		__m256d _2 = _mm256_set1_pd(2);
		__m256d _4 = _mm256_set1_pd(4);
		__m256d _0123 = _mm256_setr_pd(0, 1, 2, 3);

		__m256d _areaX = _mm256_set1_pd(area.x);
		__m256d _areaY = _mm256_set1_pd(area.y);
		__m256d _deltaX = _mm256_set1_pd((area.z - area.x) / wSize.x);
		__m256d _deltaY = _mm256_set1_pd((area.w - area.y) / wSize.y);
		__m256d _maxIterations = _mm256_set1_pd(maxIterations);

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			if (j % 4 != 0) {
				return;
			}

			__m256d _i = _mm256_set1_pd(i);

			__m256d _j = _mm256_set1_pd(j);
			_j = _mm256_add_pd(_j, _0123);

			__m256d _x = _mm256_setzero_pd();
			__m256d _y = _mm256_setzero_pd();

			__m256d _x0 = _mm256_fmadd_pd(_deltaX, _j, _areaX);
			__m256d _y0 = _mm256_fmadd_pd(_deltaY, _i, _areaY);

			__m256d _iteration = _mm256_setzero_pd();
			while (true) {

				__m256d _x2 = _mm256_mul_pd(_x, _x);
				__m256d _y2 = _mm256_mul_pd(_y, _y);

				__m256d _condition1 = _mm256_cmp_pd(_mm256_add_pd(_x2, _y2), _4, _CMP_LE_OQ);
				__m256d _condition2 = _mm256_cmp_pd(_iteration, _maxIterations, _CMP_LT_OQ);
				__m256d _loopCondition = _mm256_and_pd(_condition1, _condition2);

				if (_mm256_movemask_pd(_loopCondition) == 0) {
					break;
				}

				__m256d _nx = _mm256_add_pd(_mm256_sub_pd(_x2, _y2), _x0);
				__m256d _ny = _mm256_fmadd_pd(_mm256_mul_pd(_2, _x), _y, _y0);

				_x = _mm256_blendv_pd(_x, _nx, _loopCondition);
				_y = _mm256_blendv_pd(_y, _ny, _loopCondition);

				_iteration = _mm256_add_pd(_iteration, _mm256_and_pd(_1, _loopCondition));
			}

			__m128i _results = _mm256_cvtpd_epi32(_iteration);

			_mm_storeu_epi32(&maxIteration, _results);
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}
}

namespace Multibrot {

	template <class Type>
	void Normal(std::span<int> iterations, Vector4d area, int maxIterations, float power, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		Type deltaX = (area.z - area.x) / wSize.x;
		Type deltaY = (area.w - area.y) / wSize.y;

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			Type x = 0;
			Type y = 0;

			Type x0 = area.x + deltaX * j;
			Type y0 = area.y + deltaY * i;

			int iteration = 0;
			while (x * x + y * y <= 4 && iteration < maxIterations) {

				Type nx = std::pow(x * x + y * y, power / 2) * std::cos(power * std::atan2(y, x)) + x0;
				Type ny = std::pow(x * x + y * y, power / 2) * std::sin(power * std::atan2(y, x)) + y0;

				x = nx;
				y = ny;

				iteration++;
			}

			maxIteration = iteration;
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}



	template <class Type> requires (std::is_same<Type, float>::value)
		void SSE2(std::span<int> iterations, Vector4d area, int maxIterations, float power, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		__m128 _1 = _mm_set1_ps(1);
		__m128 _2 = _mm_set1_ps(2);
		__m128 _4 = _mm_set1_ps(4);
		__m128 _0123 = _mm_setr_ps(0, 1, 2, 3);

		__m128 _areaX = _mm_set1_ps(area.x);
		__m128 _areaY = _mm_set1_ps(area.y);
		__m128 _deltaX = _mm_set1_ps((area.z - area.x) / wSize.x);
		__m128 _deltaY = _mm_set1_ps((area.w - area.y) / wSize.y);
		__m128 _maxIterations = _mm_set1_ps(maxIterations);
		__m128 _power = _mm_set1_ps(power);
		__m128 _powerHalf = _mm_set1_ps(power / 2);

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			if (j % 4 != 0) {
				return;
			}

			__m128 _i = _mm_set1_ps(i);

			__m128 _j = _mm_set1_ps(j);
			_j = _mm_add_ps(_j, _0123);

			__m128 _x = _mm_setzero_ps();
			__m128 _y = _mm_setzero_ps();

			__m128 _x0 = _mm_fmadd_ps(_deltaX, _j, _areaX);
			__m128 _y0 = _mm_fmadd_ps(_deltaY, _i, _areaY);

			__m128 _iteration = _mm_setzero_ps();
			while (true) {

				__m128 _x2 = _mm_mul_ps(_x, _x);
				__m128 _y2 = _mm_mul_ps(_y, _y);

				__m128 _condition1 = _mm_cmp_ps(_mm_add_ps(_x2, _y2), _4, _CMP_LE_OQ);
				__m128 _condition2 = _mm_cmp_ps(_iteration, _maxIterations, _CMP_LT_OQ);
				__m128 _loopCondition = _mm_and_ps(_condition1, _condition2);

				if (_mm_movemask_ps(_loopCondition) == 0) {
					break;
				}

				__m128 _length = _mm_pow_ps(_mm_add_ps(_x2, _y2), _powerHalf);
				__m128 _angle = _mm_mul_ps(_power, _mm_atan2_ps(_y, _x));
				__m128 _nx = _mm_add_ps(_mm_mul_ps(_length, _mm_cos_ps(_angle)), _x0);
				__m128 _ny = _mm_add_ps(_mm_mul_ps(_length, _mm_sin_ps(_angle)), _y0);

				_x = _mm_blendv_ps(_x, _nx, _loopCondition);
				_y = _mm_blendv_ps(_y, _ny, _loopCondition);

				_iteration = _mm_add_ps(_iteration, _mm_and_ps(_1, _loopCondition));
			}

			__m128i _results = _mm_cvtps_epi32(_iteration);

			_mm_storeu_epi32(&maxIteration, _results);
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}

	template <class Type> requires (std::is_same<Type, double>::value)
		void SSE2(std::span<int> iterations, Vector4d area, int maxIterations, float power, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		__m128d _1 = _mm_set1_pd(1);
		__m128d _2 = _mm_set1_pd(2);
		__m128d _4 = _mm_set1_pd(4);
		__m128d _01 = _mm_setr_pd(0, 1);

		__m128d _areaX = _mm_set1_pd(area.x);
		__m128d _areaY = _mm_set1_pd(area.y);
		__m128d _deltaX = _mm_set1_pd((area.z - area.x) / wSize.x);
		__m128d _deltaY = _mm_set1_pd((area.w - area.y) / wSize.y);
		__m128d _maxIterations = _mm_set1_pd(maxIterations);
		__m128d _power = _mm_set1_pd(power);
		__m128d _powerHalf = _mm_set1_pd(power / 2);

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			if (j % 2 != 0) {
				return;
			}

			__m128d _i = _mm_set1_pd(i);

			__m128d _j = _mm_set1_pd(j);
			_j = _mm_add_pd(_j, _01);

			__m128d _x = _mm_setzero_pd();
			__m128d _y = _mm_setzero_pd();

			__m128d _x0 = _mm_fmadd_pd(_deltaX, _j, _areaX);
			__m128d _y0 = _mm_fmadd_pd(_deltaY, _i, _areaY);

			__m128d _iteration = _mm_setzero_pd();
			while (true) {

				__m128d _x2 = _mm_mul_pd(_x, _x);
				__m128d _y2 = _mm_mul_pd(_y, _y);

				__m128d _condition1 = _mm_cmp_pd(_mm_add_pd(_x2, _y2), _4, _CMP_LE_OQ);
				__m128d _condition2 = _mm_cmp_pd(_iteration, _maxIterations, _CMP_LT_OQ);
				__m128d _loopCondition = _mm_and_pd(_condition1, _condition2);

				if (_mm_movemask_pd(_loopCondition) == 0) {
					break;
				}

				__m128d _length = _mm_pow_pd(_mm_add_pd(_x2, _y2), _powerHalf);
				__m128d _angle = _mm_mul_pd(_power, _mm_atan2_pd(_y, _x));
				__m128d _nx = _mm_add_pd(_mm_mul_pd(_length, _mm_cos_pd(_angle)), _x0);
				__m128d _ny = _mm_add_pd(_mm_mul_pd(_length, _mm_sin_pd(_angle)), _y0);

				_x = _mm_blendv_pd(_x, _nx, _loopCondition);
				_y = _mm_blendv_pd(_y, _ny, _loopCondition);

				_iteration = _mm_add_pd(_iteration, _mm_and_pd(_1, _loopCondition));
			}

			__m128i _results = _mm_cvtpd_epi32(_iteration);

			(&maxIteration)[0] = _results.m128i_i32[0];
			(&maxIteration)[1] = _results.m128i_i32[1];
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}



	template <class Type> requires (std::is_same<Type, float>::value)
		void AVX2(std::span<int> iterations, Vector4d area, int maxIterations, float power, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		__m256 _1 = _mm256_set1_ps(1);
		__m256 _2 = _mm256_set1_ps(2);
		__m256 _4 = _mm256_set1_ps(4);
		__m256 _01234567 = _mm256_setr_ps(0, 1, 2, 3, 4, 5, 6, 7);

		__m256 _areaX = _mm256_set1_ps(area.x);
		__m256 _areaY = _mm256_set1_ps(area.y);
		__m256 _deltaX = _mm256_set1_ps((area.z - area.x) / wSize.x);
		__m256 _deltaY = _mm256_set1_ps((area.w - area.y) / wSize.y);
		__m256 _maxIterations = _mm256_set1_ps(maxIterations);
		__m256 _power = _mm256_set1_ps(power);
		__m256 _powerHalf = _mm256_set1_ps(power / 2);

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			if (j % 8 != 0) {
				return;
			}

			__m256 _i = _mm256_set1_ps(i);

			__m256 _j = _mm256_set1_ps(j);
			_j = _mm256_add_ps(_j, _01234567);

			__m256 _x = _mm256_setzero_ps();
			__m256 _y = _mm256_setzero_ps();

			__m256 _x0 = _mm256_fmadd_ps(_deltaX, _j, _areaX);
			__m256 _y0 = _mm256_fmadd_ps(_deltaY, _i, _areaY);

			__m256 _iteration = _mm256_setzero_ps();
			while (true) {

				__m256 _x2 = _mm256_mul_ps(_x, _x);
				__m256 _y2 = _mm256_mul_ps(_y, _y);

				__m256 _condition1 = _mm256_cmp_ps(_mm256_add_ps(_x2, _y2), _4, _CMP_LE_OQ);
				__m256 _condition2 = _mm256_cmp_ps(_iteration, _maxIterations, _CMP_LT_OQ);
				__m256 _loopCondition = _mm256_and_ps(_condition1, _condition2);

				if (_mm256_movemask_ps(_loopCondition) == 0) {
					break;
				}

				__m256 _length = _mm256_pow_ps(_mm256_add_ps(_x2, _y2), _powerHalf);
				__m256 _angle = _mm256_mul_ps(_power, _mm256_atan2_ps(_y, _x));
				__m256 _nx = _mm256_add_ps(_mm256_mul_ps(_length, _mm256_cos_ps(_angle)), _x0);
				__m256 _ny = _mm256_add_ps(_mm256_mul_ps(_length, _mm256_sin_ps(_angle)), _y0);

				_x = _mm256_blendv_ps(_x, _nx, _loopCondition);
				_y = _mm256_blendv_ps(_y, _ny, _loopCondition);

				_iteration = _mm256_add_ps(_iteration, _mm256_and_ps(_1, _loopCondition));
			}

			__m256i _results = _mm256_cvtps_epi32(_iteration);

			_mm256_storeu_epi32(&maxIteration, _results);
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}

	template <class Type> requires (std::is_same<Type, double>::value)
		void AVX2(std::span<int> iterations, Vector4d area, int maxIterations, float power, bool parallel) {

		Vector2i wSize = Window::getInstance().size;

		__m256d _1 = _mm256_set1_pd(1);
		__m256d _2 = _mm256_set1_pd(2);
		__m256d _4 = _mm256_set1_pd(4);
		__m256d _0123 = _mm256_setr_pd(0, 1, 2, 3);

		__m256d _areaX = _mm256_set1_pd(area.x);
		__m256d _areaY = _mm256_set1_pd(area.y);
		__m256d _deltaX = _mm256_set1_pd((area.z - area.x) / wSize.x);
		__m256d _deltaY = _mm256_set1_pd((area.w - area.y) / wSize.y);
		__m256d _maxIterations = _mm256_set1_pd(maxIterations);
		__m256d _power = _mm256_set1_pd(power);
		__m256d _powerHalf = _mm256_set1_pd(power / 2);

		const auto&& function = [&](int& maxIteration) {

			int index = &maxIteration - &iterations[0];
			int i = index / wSize.x;
			int j = index % wSize.x;

			if (j % 4 != 0) {
				return;
			}

			__m256d _i = _mm256_set1_pd(i);

			__m256d _j = _mm256_set1_pd(j);
			_j = _mm256_add_pd(_j, _0123);

			__m256d _x = _mm256_setzero_pd();
			__m256d _y = _mm256_setzero_pd();

			__m256d _x0 = _mm256_fmadd_pd(_deltaX, _j, _areaX);
			__m256d _y0 = _mm256_fmadd_pd(_deltaY, _i, _areaY);

			__m256d _iteration = _mm256_setzero_pd();
			while (true) {

				__m256d _x2 = _mm256_mul_pd(_x, _x);
				__m256d _y2 = _mm256_mul_pd(_y, _y);

				__m256d _condition1 = _mm256_cmp_pd(_mm256_add_pd(_x2, _y2), _4, _CMP_LE_OQ);
				__m256d _condition2 = _mm256_cmp_pd(_iteration, _maxIterations, _CMP_LT_OQ);
				__m256d _loopCondition = _mm256_and_pd(_condition1, _condition2);

				if (_mm256_movemask_pd(_loopCondition) == 0) {
					break;
				}

				__m256d _length = _mm256_pow_pd(_mm256_add_pd(_x2, _y2), _powerHalf);
				__m256d _angle = _mm256_mul_pd(_power, _mm256_atan2_pd(_y, _x));
				__m256d _nx = _mm256_add_pd(_mm256_mul_pd(_length, _mm256_cos_pd(_angle)), _x0);
				__m256d _ny = _mm256_add_pd(_mm256_mul_pd(_length, _mm256_sin_pd(_angle)), _y0);

				_x = _mm256_blendv_pd(_x, _nx, _loopCondition);
				_y = _mm256_blendv_pd(_y, _ny, _loopCondition);

				_iteration = _mm256_add_pd(_iteration, _mm256_and_pd(_1, _loopCondition));
			}

			__m128i _results = _mm256_cvtpd_epi32(_iteration);

			_mm_storeu_epi32(&maxIteration, _results);
		};

		if (parallel) {
			std::for_each(std::execution::par_unseq, iterations.begin(), iterations.end(), function);
		}
		else {
			std::for_each(std::execution::seq, iterations.begin(), iterations.end(), function);
		}
	}
}
