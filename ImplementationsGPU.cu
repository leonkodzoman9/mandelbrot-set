#include "ImplementationsGPU.cuh"



int* iterationsGPU = nullptr;



template <class Type>
__global__ void MandelbrotCUDAImpl(int* iterations, double4 area, int maxIterations, int2 wSize) {

	int j = blockDim.x * blockIdx.x + threadIdx.x;
	int i = blockDim.y * blockIdx.y + threadIdx.y;

	if (j >= wSize.x || i >= wSize.y) { return; }

	Type deltaX = ((Type)area.z - (Type)area.x) / (Type)wSize.x;
	Type deltaY = ((Type)area.w - (Type)area.y) / (Type)wSize.y;

	Type x = 0;
	Type y = 0;

	Type x0 = (Type)area.x + deltaX * j;
	Type y0 = (Type)area.y + deltaY * i;

	int iteration = 0;
	while (x * x + y * y <= (Type)4 && iteration < maxIterations) {

		Type nx = x * x - y * y + x0;
		Type ny = (Type)2 * x * y + y0;

		x = nx;
		y = ny;

		iteration++;
	}

	__syncthreads();

	iterations[i * wSize.x + j] = iteration;
}

void Mandelbrot::CUDA(int* iterations, int pixelCount, double4 area, int maxIterations, int2 wSize, bool doublePrecision) {

	if (iterationsGPU == nullptr) {
		cudaMalloc(&iterationsGPU, pixelCount * sizeof(int));
	}

	dim3 threads(32, 8);
	dim3 blocks(wSize.x / threads.x + 1, wSize.y / threads.y + 1);

	if (!doublePrecision) {
		MandelbrotCUDAImpl<float> << <blocks, threads >> > (iterationsGPU, area, maxIterations, wSize);
		cudaMemcpy(iterations, iterationsGPU, pixelCount * sizeof(int), cudaMemcpyDeviceToHost);
	}
	else {
		MandelbrotCUDAImpl<double> << <blocks, threads >> > (iterationsGPU, area, maxIterations, wSize);
		cudaMemcpy(iterations, iterationsGPU, pixelCount * sizeof(int), cudaMemcpyDeviceToHost);
	}
}



template <class Type>
__global__ void MultibrotCUDAImpl(int* iterations, double4 area, int maxIterations, int2 wSize, float power) {

	int j = blockDim.x * blockIdx.x + threadIdx.x;
	int i = blockDim.y * blockIdx.y + threadIdx.y;

	if (j >= wSize.x || i >= wSize.y) { return; }

	Type deltaX = ((Type)area.z - (Type)area.x) / (Type)wSize.x;
	Type deltaY = ((Type)area.w - (Type)area.y) / (Type)wSize.y;

	Type x = 0;
	Type y = 0;

	Type x0 = (Type)area.x + deltaX * j;
	Type y0 = (Type)area.y + deltaY * i;

	int iteration = 0;
	while (x * x + y * y <= (Type)4 && iteration < maxIterations) {

		Type nx = pow(x * x + y * y, power * (Type)0.5) * cos(power * atan2(y, x)) + x0;
		Type ny = pow(x * x + y * y, power * (Type)0.5) * sin(power * atan2(y, x)) + y0;

		x = nx;
		y = ny;

		iteration++;
	}

	__syncthreads();

	iterations[i * wSize.x + j] = iteration;
}

void Multibrot::CUDA(int* iterations, int pixelCount, double4 area, int maxIterations, int2 wSize, float power, bool doublePrecision) {

	if (iterationsGPU == nullptr) {
		cudaMalloc(&iterationsGPU, pixelCount * sizeof(int));
	}

	dim3 threads(32, 8);
	dim3 blocks(wSize.x / threads.x + 1, wSize.y / threads.y + 1);

	if (!doublePrecision) {
		MultibrotCUDAImpl<float> << <blocks, threads >> > (iterationsGPU, area, maxIterations, wSize, power);
		cudaMemcpy(iterations, iterationsGPU, pixelCount * sizeof(int), cudaMemcpyDeviceToHost);
	}
	else {
		MultibrotCUDAImpl<double> << <blocks, threads >> > (iterationsGPU, area, maxIterations, wSize, power);
		cudaMemcpy(iterations, iterationsGPU, pixelCount * sizeof(int), cudaMemcpyDeviceToHost);
	}
}

