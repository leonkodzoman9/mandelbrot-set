#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <fstream>

#include <cmath>
#include <ctime>

#include <chrono>
#include <thread>
#include <algorithm>
#include <random>
#include <numeric>
#include <numbers>
#include <execution>

#include <string>
#include <sstream>

#include <array>
#include <vector>
#include <map>
#include <unordered_map>
#include <queue>
#include <set>
#include <span>

#include <type_traits>
#include <emmintrin.h>

